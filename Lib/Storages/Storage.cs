﻿using System.Collections.Generic;

namespace Storages
{
    public class Storage : IStorage
    {
        private readonly Dictionary<string, object> _keyToObjects = new Dictionary<string, object>();

        public void Put<T>(string key, T value)
        {
            _keyToObjects[key] = value;
        }

        public T Get<T>(string key)
        {
            var targetType = typeof(T);

            object obj;
            if (_keyToObjects.TryGetValue(key, out obj))
            {
                if (obj.GetType() == targetType)
                {
                    return (T)obj;
                }
            }

            return default(T);
        }

        public bool Contains(string key)
        {
            return _keyToObjects.ContainsKey(key);
        }
    }
}