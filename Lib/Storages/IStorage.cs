﻿namespace Storages
{
    public interface IStorage
    {
        void Put<T>(string key, T value);
        T Get<T>(string key);
        bool Contains(string key);
    }
}