﻿using System;
using Properties;

namespace Storages.Properties
{
    internal class StoredProperty<T> : Property<T>
        where T : IEquatable<T>
    {
        private readonly string _key;
        private readonly IStorage _storage;

        public StoredProperty(IStorage storage, string key)
            : base(default(T))
        {
            _key = key;
            _storage = storage;
            InternalValue = _storage.Get<T>(_key);
        }

        public StoredProperty(IStorage storage, string key, T defaultValue)
            : base(defaultValue)
        {
            _key = key;
            _storage = storage;

            if (_storage.Contains(key))
            {
                InternalValue = _storage.Get<T>(_key);
            }
            else
            {
                InternalValue = defaultValue;
                _storage.Put(_key, defaultValue);
            }
        }

        protected override void OnChanged(T previous, T current)
        {
            _storage.Put(_key, current);
        }
    }
}