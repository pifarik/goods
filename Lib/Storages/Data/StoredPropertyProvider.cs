﻿using System;
using Properties;

namespace Storages.Properties
{
    public class StoredPropertyProvider : IPropertyProvider
    {
        private readonly IStorage _storage;

        public StoredPropertyProvider(IStorage storage)
        {
            _storage = storage;
        }

        public Property<TType> GetProperty<TType>(string key, TType defaultValue)
            where TType : IEquatable<TType>
        {
            return new StoredProperty<TType>(_storage, key, defaultValue);
        }

        public Property<TType> GetProperty<TType>(string key)
            where TType : IEquatable<TType>
        {
            return new StoredProperty<TType>(_storage, key);
        }
    }
}