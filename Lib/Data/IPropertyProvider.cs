﻿using System;

namespace Properties
{
    public interface IPropertyProvider
    {
        Property<T> GetProperty<T>(string key, T defaultValue)
            where T : IEquatable<T>;

        Property<T> GetProperty<T>(string key)
            where T : IEquatable<T>;
    }
}