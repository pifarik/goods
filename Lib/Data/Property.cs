﻿using System;

namespace Properties
{
    public abstract class Property<T>
        where T : IEquatable<T>
    {
        protected T InternalValue;

        protected Property(T value)
        {
            InternalValue = value;
        }

        public T Value
        {
            get { return InternalValue; }
            set
            {
                if (InternalValue.Equals(value))
                {
                    return;
                }

                var previous = InternalValue;
                InternalValue = value;

                OnChanged(previous, InternalValue);
            }
        }

        protected abstract void OnChanged(T previous, T current);

        public static implicit operator T(Property<T> property)
        {
            return property.Value;
        }
    }
}