﻿using Properties;

namespace App
{
    // Settings for some area
    class ApplicationSettings
    {
        public readonly Property<int> FirstPlay;


        public ApplicationSettings(Settings settings)
        {
            FirstPlay = settings.ForUser.GetProperty<int>("Key");
        }
    }

    // Settings for some area
    class GamerSettings
    {
        public readonly Property<int> FirstPlay;

        public GamerSettings(Settings settings)
        {
            FirstPlay = settings.ForUser.GetProperty<int>("FirstPlay");
        }
    }


    // Scopes
    internal class Settings
    {
        public IPropertyProvider ForUser { get; set; }
        public IPropertyProvider ForApplication { get; set; }
        public IPropertyProvider ForSession { get; set; }
        public IPropertyProvider ForLunch { get; set; }
    }
}
